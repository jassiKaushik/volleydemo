package com.demo.vollydemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.demo.model.Test;
import com.demo.model.Token;
import com.demo.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

import Lisnter.OnResponse;
import Util.Constants;
import Util.WebserviceUtil;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity implements OnResponse {

    @Bind(R.id.txtResponse)
    TextView txtResponse;

    @Bind(R.id.imageView)
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // findViewById(R.id.btnStringRequest).setOnClickListener(this);
        Toast.makeText(getApplicationContext(), "hiii", Toast.LENGTH_LONG).show();
        ButterKnife.bind(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

   /* @Override
    public void onClick(View v) {
      if(v.getId()==R.id.btnStringRequest) {

      }
    }*/

    @OnClick(R.id.btnStringRequest)
    protected void Toast() {
        WebserviceUtil.stringRequest(this);
    }

    @OnClick(R.id.btnJsonRequest)
    protected void callJsonWebservice() {
        WebserviceUtil.jsonRequest(this);
    }

    @OnClick(R.id.btnJsonArrary)
    protected void callJsonArratWebService() {
        WebserviceUtil.jsonArrayRequest(this);
    }

    @OnClick(R.id.btnImageReqt)
    protected void callImageWebService() {
        ImageLoader imageLoader = Application.getInstance().getmImageLoader();
        /*imageLoader.get(Constants.URL_IMAGE, ImageLoader.getImageListener(
                imageView, R.drawable.congratulations1, R.drawable.congratulations1));*/
        // imageView.setImageUrl(Constants.URL_IMAGE,imageLoader);
        imageLoader.get(Constants.URL_IMAGE, ImageLoader.getImageListener(
                imageView, R.drawable.congratulations1, R.drawable.congratulations1));


    }

    @OnClick(R.id.textView)
    protected void callJsonObjectHeaderWebService() {
        WebserviceUtil.jsonObjectRequestwithheader(this);
    }


    @OnClick(R.id.btnPostObject)

    protected void callPostMethodWebservice() {
        try {
            Gson gson = new Gson();
            String json = gson.toJson(new Test("jassi", "jassi", "jassikaushik@yahoo.com"));
            JSONObject jsonObject = new JSONObject(json);
            WebserviceUtil.callPostMethodWebservice(jsonObject, this);
        } catch (JSONException json) {

        }


    }

    @OnClick(R.id.btnHeaderPost)
    protected void callPostMethodWebserviceWithHeader(){
        try {
            Gson gson=new Gson();
            String json=gson.toJson(new Token("112","111"));
            JSONObject jsonObject=new JSONObject(json);
            WebserviceUtil.callPostMethodWebserviceWithHeader(jsonObject, this);
        } catch (Exception e) {

        }

    }

    @OnClick(R.id.btnCancelRequest)
    protected  void cancelRequest() {
        WebserviceUtil.cancelJsonPostRequest("req");
    }


    @Override
    public void onSuccess(String success) {
        txtResponse.setText(success);
    }


    @Override
    public void OnFailure() {
        txtResponse.setText("something went wrong");

    }

    @Override
    public void onSuccess(JSONObject success) {
      /*  Gson gson=new Gson();
        Type type = new TypeToken<User>() {}.getType();
        User user=gson.fromJson(success.toString(),type);*/

        txtResponse.setText(success.toString());
    }

    @Override
    public void onSuccess(JSONArray success) {
        txtResponse.setText(success.toString());

    }
}
