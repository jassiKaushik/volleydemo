package com.demo.vollydemo;

import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import Util.LRUImageLoader;

/**
 * Created by craterzone on 17/6/16.
 */
public class Application extends android.app.Application {

    private RequestQueue requestQueue;
    private static Application _instance=null;
    private static String TAG=Application.class.getSimpleName();
    private ImageLoader mImageLoader;

    @Override
    public void onCreate() {
        super.onCreate();
        _instance=this;
    }

    public static synchronized Application getInstance() {
        if(_instance==null) {
            _instance=new Application();
        }

        return _instance;

    }

    private RequestQueue getQueue() {
        if(requestQueue==null) {
         //   Log.d("crash",getApplicationContext()+"");
            requestQueue=Volley.newRequestQueue(getApplicationContext());
        }
        return requestQueue;

    }

    public ImageLoader getmImageLoader(){
        getQueue();
        if(mImageLoader==null) {
            mImageLoader=new ImageLoader(requestQueue,new LRUImageLoader());
        }
        return  mImageLoader;
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public <T> void addToQueue(Request<T>req,String tag){
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);

        getQueue().add(req);

    }

    public void cancelRequest(String tag) {
        Application.getInstance().getQueue().cancelAll(tag);
    }

}
