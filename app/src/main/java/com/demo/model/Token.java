package com.demo.model;

/**
 * Created by craterzone on 18/6/16.
 */

public class Token
{
    public Token(String deviceToken, String userId) {
        this.deviceToken = deviceToken;
        this.userId = userId;
    }

    private String deviceToken;

    private String userId;

    public String getDeviceToken ()
    {
        return deviceToken;
    }

    public void setDeviceToken (String deviceToken)
    {
        this.deviceToken = deviceToken;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [deviceToken = "+deviceToken+", userId = "+userId+"]";
    }
}