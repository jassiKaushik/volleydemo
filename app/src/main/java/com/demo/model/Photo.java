package com.demo.model;

/**
 * Created by craterzone on 18/6/16.
 */
public class Photo
{
    private Photo_big photo_big;

    private Photo_small photo_small;

    private String photo_id;

    public Photo_big getPhoto_big ()
    {
        return photo_big;
    }

    public void setPhoto_big (Photo_big photo_big)
    {
        this.photo_big = photo_big;
    }

    public Photo_small getPhoto_small ()
    {
        return photo_small;
    }

    public void setPhoto_small (Photo_small photo_small)
    {
        this.photo_small = photo_small;
    }

    public String getPhoto_id ()
    {
        return photo_id;
    }

    public void setPhoto_id (String photo_id)
    {
        this.photo_id = photo_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [photo_big = "+photo_big+", photo_small = "+photo_small+", photo_id = "+photo_id+"]";
    }
}
