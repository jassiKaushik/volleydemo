package com.demo.model;

/**
 * Created by craterzone on 18/6/16.
 */
public class TelegramUsers
{
    private String cCode;

    private String id;

    private String sex;

    private String phone;

    private String username;

    private String visibility;

    private String dob;

    private String userId;

    private String name;

    private String userHash;

    private Photo photo;

    public String getCCode ()
    {
        return cCode;
    }

    public void setCCode (String cCode)
    {
        this.cCode = cCode;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getSex ()
    {
        return sex;
    }

    public void setSex (String sex)
    {
        this.sex = sex;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getUsername ()
{
    return username;
}

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getVisibility ()
    {
        return visibility;
    }

    public void setVisibility (String visibility)
    {
        this.visibility = visibility;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUserHash ()
    {
        return userHash;
    }

    public void setUserHash (String userHash)
    {
        this.userHash = userHash;
    }

    public Photo getPhoto ()
    {
        return photo;
    }

    public void setPhoto (Photo photo)
    {
        this.photo = photo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [cCode = "+cCode+", id = "+id+", sex = "+sex+", phone = "+phone+", username = "+username+", visibility = "+visibility+", dob = "+dob+", userId = "+userId+", name = "+name+", userHash = "+userHash+", photo = "+photo+"]";
    }
}
