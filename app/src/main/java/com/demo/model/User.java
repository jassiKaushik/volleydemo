package com.demo.model;

/**
 * Created by craterzone on 18/6/16.
 */
public class User
{
    private String id;

    private String cCode;

    private String username;

    private String phone;

    private String sex;

    private String visibility;

    private String name;

    private String userId;

    private String dob;

    private String userHash;

    private Photo photo;





    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getCCode ()
    {
        return cCode;
    }

    public void setCCode (String cCode)
    {
        this.cCode = cCode;
    }

    public String getUsername ()
{
    return username;
}

    public void setUsername (String username)
    {
        this.username = username;
    }

    public String getPhone ()
    {
        return phone;
    }

    public void setPhone (String phone)
    {
        this.phone = phone;
    }

    public String getSex ()
    {
        return sex;
    }

    public void setSex (String sex)
    {
        this.sex = sex;
    }

    public String getVisibility ()
    {
        return visibility;
    }

    public void setVisibility (String visibility)
    {
        this.visibility = visibility;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getUserId ()
    {
        return userId;
    }

    public void setUserId (String userId)
    {
        this.userId = userId;
    }

    public String getDob ()
    {
        return dob;
    }

    public void setDob (String dob)
    {
        this.dob = dob;
    }

    public String getUserHash ()
    {
        return userHash;
    }

    public void setUserHash (String userHash)
    {
        this.userHash = userHash;
    }

    public Photo getPhoto ()
    {
        return photo;
    }

    public void setPhoto (Photo photo)
    {
        this.photo = photo;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [id = "+id+", cCode = "+cCode+", username = "+username+", phone = "+phone+", sex = "+sex+", visibility = "+visibility+", name = "+name+", userId = "+userId+", dob = "+dob+", userHash = "+userHash+", photo = "+photo+"]";
    }
}


