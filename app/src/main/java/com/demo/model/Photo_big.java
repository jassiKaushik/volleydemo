package com.demo.model;

/**
 * Created by craterzone on 18/6/16.
 */
public class Photo_big
{
    private String local_id;

    private String volume_id;

    private String iv;

    private String dc_id;

    private String secret;

    private String key;

    public String getLocal_id ()
    {
        return local_id;
    }

    public void setLocal_id (String local_id)
    {
        this.local_id = local_id;
    }

    public String getVolume_id ()
    {
        return volume_id;
    }

    public void setVolume_id (String volume_id)
    {
        this.volume_id = volume_id;
    }

    public String getIv ()
    {
        return iv;
    }

    public void setIv (String iv)
    {
        this.iv = iv;
    }

    public String getDc_id ()
    {
        return dc_id;
    }

    public void setDc_id (String dc_id)
    {
        this.dc_id = dc_id;
    }

    public String getSecret ()
    {
        return secret;
    }

    public void setSecret (String secret)
    {
        this.secret = secret;
    }

    public String getKey ()
    {
        return key;
    }

    public void setKey (String key)
    {
        this.key = key;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [local_id = "+local_id+", volume_id = "+volume_id+", iv = "+iv+", dc_id = "+dc_id+", secret = "+secret+", key = "+key+"]";
    }
}

