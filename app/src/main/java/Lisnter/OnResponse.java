package Lisnter;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by craterzone on 17/6/16.
 */
public interface OnResponse {
     void onSuccess(String success);
     void onSuccess(JSONObject success);
     void onSuccess(JSONArray success);

     void OnFailure();

}

