package Util;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.Request.Method;
import com.demo.vollydemo.Application;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import Lisnter.OnResponse;


/**
 * Created by craterzone on 17/6/16.
 */
public class WebserviceUtil {

    private static String TAG = WebserviceUtil.class.getName();

    /**
     * @param listner callback to activity
     *                description : call Get webservice for string
     */
    public static void stringRequest(final OnResponse listner) {
        StringRequest stringRequest = new StringRequest(Method.GET, Constants.URL_STRING_REQ, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                VolleyLog.d(TAG, "Response: " + s);
                listner.onSuccess(s);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                listner.OnFailure();
            }
        });
        Application.getInstance().addToQueue(stringRequest, TAG);

    }

    /**
     * @param lisnter callback to activity
     *                description : call get webservice for json
     */

    public static void jsonRequest(final OnResponse lisnter) {
        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                Constants.URL_JSON_OBJECT, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        lisnter.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                lisnter.OnFailure();
            }
        });
        Application.getInstance().addToQueue(jsonObjReq, TAG);

    }

    /**
     * @param lisnter callback to activity
     *                description : call get webservice for json array
     */
    public static void jsonArrayRequest(final OnResponse lisnter) {
        JsonArrayRequest jsonObjReq = new JsonArrayRequest(Method.GET,
                Constants.URL_JSON_ARRAY, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        Log.d(TAG, response.toString());
                        lisnter.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                lisnter.OnFailure();
            }
        });
        Application.getInstance().addToQueue(jsonObjReq, TAG);

    }

    public static void jsonObjectRequestwithheader(final OnResponse lisnter) {

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Method.GET,
                Constants.URL_URL, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d(TAG, response.toString());
                        lisnter.onSuccess(response);
                    }
                }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                lisnter.OnFailure();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Accept", "application/json");
                header.put("uniqueId", "IN_9528414431");

                return header;
            }


        };
        Application.getInstance().addToQueue(jsonObjReq, "req");
    }


    public static void callPostMethodWebservice(JSONObject object, final OnResponse lisnter) {
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Method.POST, Constants.URL_Post, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                lisnter.onSuccess(response);
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                lisnter.OnFailure();
            }
        });
        Application.getInstance().addToQueue(jsonObjectRequest, TAG);

    }

    public static void callPostMethodWebserviceWithHeader(JSONObject object, final OnResponse lisnter) {
        JsonObjectRequestor jsonObjectRequest = new JsonObjectRequestor(Method.POST, Constants.URL_Tel_Post, object, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "");
                lisnter.onSuccess("");
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                VolleyLog.d(TAG, "Error: " + error.getMessage());
                lisnter.OnFailure();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> header = new HashMap<>();
                header.put("Accept", "application/json");
                header.put("userId", "bfa6655a8f6c3bcbfa1c3bd810feb002");

                return header;
            }


        };
        Application.getInstance().addToQueue(jsonObjectRequest, TAG);
    }

    public static void cancelJsonPostRequest(String tag) {
        Application.getInstance().cancelRequest(tag);
    }

}
